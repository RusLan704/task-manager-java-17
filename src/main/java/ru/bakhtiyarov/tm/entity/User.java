package ru.bakhtiyarov.tm.entity;

import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.Serializable;

public class User extends AbstractEntity implements Serializable {

    private String login = "";

    private String passwordHash = "";

    private String email = "";

    private String fistName = "";

    private String lastName = "";

    private String middleName = "";

    private Role role = Role.USER;

    private Boolean locked = false;

    public Role getRole() {
        return role;
    }

    public String getFistName() {
        return fistName;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

}