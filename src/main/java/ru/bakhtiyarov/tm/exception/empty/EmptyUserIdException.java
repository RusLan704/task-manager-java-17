package ru.bakhtiyarov.tm.exception.empty;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }

}
