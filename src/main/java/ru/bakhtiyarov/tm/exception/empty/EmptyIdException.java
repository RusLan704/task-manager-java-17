package ru.bakhtiyarov.tm.exception.empty;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
