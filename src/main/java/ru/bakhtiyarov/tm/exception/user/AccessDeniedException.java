package ru.bakhtiyarov.tm.exception.user;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}