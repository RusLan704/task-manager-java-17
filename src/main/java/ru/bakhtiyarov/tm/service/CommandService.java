package ru.bakhtiyarov.tm.service;

import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.service.ICommandService;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}
