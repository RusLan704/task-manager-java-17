package ru.bakhtiyarov.tm.command.user;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.User;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.USER_VIEW_PROFILE;
    }

    @Override
    public String description() {
        return "Show user profile.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        final String email = user.getEmail();
        if (email != null && !email.isEmpty()) System.out.println("EMAIL: " + email);
        final String firstName = user.getFirstName();
        if (firstName != null && !firstName.isEmpty()) System.out.println("FIRST NAME: " + firstName);
        final String lastName = user.getLastName();
        if (lastName != null && !lastName.isEmpty()) System.out.println("LAST NAME: " + lastName);
        final String middleName = user.getMiddleName();
        if (middleName != null && !middleName.isEmpty()) System.out.println("MIDDLE NAME: " + middleName);
        System.out.println("[OK]");
    }

}