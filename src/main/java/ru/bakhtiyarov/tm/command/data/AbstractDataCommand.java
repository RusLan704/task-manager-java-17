package ru.bakhtiyarov.tm.command.data;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.dto.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {


    protected final String FILE_BINARY = "./data.bin";

    protected final String FILE_BASE64 = "./data.base64";

    protected final String FILE_XML = ".data.xml";

    protected final String FILE_JSON = "./data.json";

    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(final Domain domain) {
        if(domain == null) return;
        serviceLocator.getAuthService().logout();
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
    }

}
