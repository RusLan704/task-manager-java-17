package ru.bakhtiyarov.tm.command.data;

import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String description() {
        return "Load base64 data from file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final String base64date = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte[] decodeData = new BASE64Decoder().decodeBuffer(base64date);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
        final ObjectInputStream objectOutputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectOutputStream.readObject();
        setDomain(domain);
        objectOutputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
