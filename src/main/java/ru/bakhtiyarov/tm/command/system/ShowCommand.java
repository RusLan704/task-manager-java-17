package ru.bakhtiyarov.tm.command.system;

import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public class ShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command : commands) System.out.println(command.name());
    }

}