package ru.bakhtiyarov.tm.command.system;

import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command.name() + ": " + command.description());
        }
        System.out.println("[OK]");
    }

}
