package ru.bakhtiyarov.tm.api.repository;

import ru.bakhtiyarov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommandList();

}

